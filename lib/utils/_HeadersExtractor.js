'use strict';

const lodash = require('lodash');
const { isNotNil, isPlainObject } = require('../../engine/utils');

function HeadersExtractor(descriptor) {
  this._descriptor = descriptor;
}

HeadersExtractor.prototype.getEntrypoints = function() {
  if (this._entrypoints) {
    return this._entrypoints;
  }
  const hashMap = {};
  const { rules } = this._descriptor || {};
  lodash.forOwn(rules, function(rule, key) {
    if (isPlainObject(rule)) {
      if (lodash.isArray(rule.path) && rule.path.length > 0) {
        hashMap[rule.path[0]] = null;
      }
    }
  })
  this._entrypoints = lodash.keys(hashMap);
  return this._entrypoints;
}

HeadersExtractor.prototype.extractHeaders = function(target, headers, req) {
  const that = this;
  const { rules } = this._descriptor || {};
  target = target || {};
  if (rules) {
    lodash.forOwn(rules, function(rule, key) {
      let value = that._getHeader(key, headers, req);
      if (isNotNil(value) && rule) {
        if (rule.type === "boolean") {
          value = (value === "true");
        }
        if (rule.type === "integer") {
          value = parseInt(value) || null;
        }
        if (rule.type === "float") {
          value = parseFloat(value) || null;
        }
        lodash.set(target, rule.path, value || rule.default);
      }
    })
  }
  return target;
}

HeadersExtractor.prototype._getHeader = function(key, headers, req) {
  if (!key) {
    return null;
  }
  if (headers) {
    return headers[key];
  }
  if (req) {
    return req.get(key);
  }
  return null;
}

module.exports = { HeadersExtractor };
