'use strict';

const lodash = require('lodash');
const { Sequelize, Op } = require('sequelize');

const { DB } = require('../models')

function AbstractManager(params) {
  const { db } = params || {};
  this._db = (db instanceof DB) ? db : null;
}

//-------------------------------------------------------------------------------------------------

AbstractManager.prototype.getDB = function(more) {
  return this._db;
}

AbstractManager.prototype.getTransaction = async function(more) {
  const dbAccessor = this.getDB(more);
  return await dbAccessor.handler.transaction({
    transaction: more && more.transaction ? more.transaction : undefined,
    isolationLevel: Sequelize.Transaction.SERIALIZABLE
  });
}

AbstractManager.prototype.dropTables = async function(more) {
  const dbAccessor = this.getDB(more);
  return await dbAccessor.drop();
}

AbstractManager.prototype.resetModels = async function(more) {
  const dbAccessor = this.getDB(more);
  return await dbAccessor.sync({ force: true });
}

AbstractManager.prototype.closeConnection = async function(more) {
  const dbAccessor = this.getDB(more);
  return await dbAccessor.close();
}

//-------------------------------------------------------------------------------------------------

AbstractManager.prototype.__schemaOfFields__ = {}

//-------------------------------------------------------------------------------------------------

module.exports = { AbstractManager };

