'use strict';

function BusinessOperator(params) {
  const { accessTokenObject: ato } = params || {};
  //
  Object.defineProperty(this, "id", {
    get: function() {
      return ato && (ato.holderId || ato.userId) || undefined;
    },
    set: function(val) {
      // noop
    }
  });
}

module.exports = { BusinessOperator };
