'use strict';

const { DB } = require("../../engine/models");

function Service (params = {}) {
  const { packageName, sandboxConfig } = params;

  const db = new DB(sandboxConfig.dbParameters);

  this.getDB = function() {
    return db;
  }
}

Service.referenceHash = {}

module.exports = Service;
