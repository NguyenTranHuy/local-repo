'use strict';

const path = require('path');

const contextPath = '/api';

const conf = {
  application: {
    contextPath: contextPath,
    dbParameters: {
      "dialect": "mysql",
      "database": "theater",
      "username": "root",
      "password": "newpassword",
      "host": "localhost",
      "port": 3306
    },
  },
  plugins: {
    appRestfront: {
      priority: 100,
      contextPath: contextPath,
      mappingStore: {
        'inventoryManager': path.join(__dirname, '../lib/mappings/restfront/web-inventory-manager.js'),
      },
    },
    appWebweaver: {
      cors: {
        enabled: true,
        mode: 'simple',
      },
    },
    appWebserver: {
      host: process.env['appWebserver_host'] || "0.0.0.0",
      port: process.env['appWebserver_port'] || 8080
    },
  },
};

module.exports = conf;
