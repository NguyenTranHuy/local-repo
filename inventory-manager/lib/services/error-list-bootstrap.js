'use strict';

function Service (params = {}) {
  const { packageName, sandboxConfig, errorManager } = params;
  errorManager.register(packageName, {
    errorCodes: sandboxConfig.errorCodes
  });
  //
  const errorBuilder = errorManager.getErrorBuilder(packageName);
  //
  this.wrapError = function(err) {
    return errorBuilder.newError(err.name, {
      payload: err.payload
    });
  }
}

Service.referenceHash = {
  errorManager: 'app-errorlist/manager'
}

module.exports = Service;
