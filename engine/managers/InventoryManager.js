'use strict';

const util = require('util');
const { AbstractManager } = require('./AbstractManager');

function InventoryManager(params) {
  AbstractManager.call(this, params);
}

util.inherits(InventoryManager, AbstractManager);

//-------------------------------------------------------------------------------------------------
// Ticket

InventoryManager.prototype.findTickets = async function(criteria, more) {
  const result = await this.getDB(more).Ticket.findAndCountAll();
  return result;
}
InventoryManager.prototype.create = async function(Object, more) {
  const record = await this._create(Object);
  return record.toBLOW();
}
InventoryManager.prototype._create = async function(Object, more) {
  const dbAccessor = this.getDB(more);
  //
  return await dbAccessor.Ticket.create(injectId(Object), more);
}

//-------------------------------------------------------------------------------------------------
// Type

InventoryManager.prototype.findTypes = async function(criteria, more) {
  const result = await this.getDB(more).Type.findAndCountAll();
  return result;
}

//-------------------------------------------------------------------------------------------------
// Movie

InventoryManager.prototype.findMovies = async function(criteria, more) {
  const result = await this.getDB(more).Movie.findAndCountAll();
  return result;
}

//-------------------------------------------------------------------------------------------------
//Product

InventoryManager.prototype.findProducts = async function(criteria, more) {
  const result = await this.getDB(more).Product.findAndCountAll();
  return result;
}

InventoryManager.prototype.getProduct = async function(productId, more) {
  const record = await this.getDB(more).Product.findAndCountAll({
    where: {
      id: productId,
    }
  });
  return record;
}

//-------------------------------------------------------------------------------------------------
//Provider

InventoryManager.prototype.findProviders = async function(criteria, more) {
  const result = await this.getDB(more).Provider.findAndCountAll();
  return result;
}

//-------------------------------------------------------------------------------------------------

InventoryManager.prototype.findProductForms = async function(criteria, more) {
  const result = await this.getDB(more).ProductForm.findAndCountAll();
  return result;
}

InventoryManager.prototype.getProductForm = async function(categoryId, more) {
  const record = await this.getDB(more).ProductForm.findAndCountAll({
    where: {
      categoryId: categoryId,
    }
  });
  return record;
}

//-------------------------------------------------------------------------------------------------

InventoryManager.prototype.findCategories = async function(criteria, more) {
  const result = await this.getDB(more).Category.findAndCountAll();
  return result;
}

InventoryManager.prototype.getCategory = async function(categoryId, more) {
  const record = await this.getDB(more).Category.findOne({
    where: {
      categoryId: categoryId,
    }
  });
  return record;
}

//-------------------------------------------------------------------------------------------------

InventoryManager.prototype.findProductFormTypes = async function(criteria, more) {
  const result = await this.getDB(more).ProductFormType.findAndCountAll();
  return result;
}

InventoryManager.prototype.getProductFormType = async function(productFormId, more) {
  const record = await this.getDB(more).ProductFormType.findAndCountAll({
    where: {
      productFormId: productFormId,
    }
  });
  return record;
}

//-------------------------------------------------------------------------------------------------

module.exports = { InventoryManager }
