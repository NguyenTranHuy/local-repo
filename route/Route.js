'use strict';

const lodash = require('lodash');
const { Sequelize } = require('sequelize');
const { InventoryManager } = require('../engine/managers/InventoryManager');
let route = Sequelize.initRoute;

const initRoute =(app) => {
    route.get('/products',InventoryManager.getProducts);
    return app.use('/api/v1/',route);
}

module.exports = { initRoute };