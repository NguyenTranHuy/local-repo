'use strict';

var path = require('path');

module.exports = {
  devebot: {
    mode: "silent"
  },
  logger: {
    transports: {
      console: {
        type: 'console',
        level: 'debug',
        json: false,
        timestamp: true,
        colorize: false
      }
    }
  }
};
