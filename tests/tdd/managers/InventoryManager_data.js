'use strict'

const sample = {
  genders: [
    {
      id: "xy",
      name: "Nam",
      "__metadata__":{

      }
    },
    {
       id: "xx",
       name: "Nữ",
       
    }
  ],
  benefits: [
    {
      id: "bf1",
      name: "Bảo hiểm ngoại trú",
      "__metadata__": {
      }  
    },
    {
      id: "bf2",
      name: "Nha khoa"
      
    },
    {
      id: "bf3",
      name: "Trợ cấp nằm viện"
    },
    {
      id: "bf4",
      name: "Thai sản"
    }
  ],
  informationTypes:[
    {
      id: "type1",
      name: "Ưu điểm",
      "__metadata__": {
      }  
    },
    {
      id: "type2",
      name: "Đối tượng tham gia",
    },
    {
      id: "type3",
      name: "Điều kiện bảo hiểm",
    },
    {
      id: "type4",
      name: "Trường hợp nhận bảo hiểm",
    },
    {
      id: "type5",
      name: "Yêu cầu bồi thường",
    },
    {
      id: "type6",
      name: "Mức Quyền Lợi Cao Nhất",
    },
    {
      id: "type7",
      name: "Độ tuổi",
    },
  ],
  productPackages: [
    {
      id: "1cu",
      name: "Bạc",
      "__metadata__": {
      }
    },
    {
      id: "2ag",
      name: "Bạc"
    },
    {
      id: "3ti",
      name: "Titan"
    },
    {
      id: "4au",
      name: "Vàng"
    },
    {
      id: "5pt",
      name: "Bạch Kim"
    },
  ],
  productTypes:[
    {
      "id": "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
      "name": "Bảo hiểm Phi Nhân Thọ",
      
      "__metadata__": {
      }
    }
  ],
  providers: [
    {
      "id": "5c0d38e1-c06d-403e-b714-fa7767de866e",
      "name": "VBI",
      "__metadata__": {
      }
    }
  ],

  informationProducts: [
    {     
      providerName: "VBI", 
      productId: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb670",    
      productName: "Bảo hiểm Sức khoẻ",
      infoTypeId: "type2",
      infoTypeName: "Đối tượng tham gia",
      description: "Công dân Việt Nam",
      __metadata__: {

      }
    },
    // {
    //   providerName: "VBI",
    //   name: "Bảo hiểm Sức khoẻ",
    //   infoTypeName: "Đối tượng tham gia",
    //   description: "Người nước ngoài sống hợp pháp tại Việt Nam",
    // },
    // {
    //   providerName: "VBI",
    //   name: "Bảo hiểm Sức khoẻ",
    //   infoTypeName: "Độ tuổi",
    //   description: "2 tháng -> 65 tuổi",
    // },
  ],

  products: [
    {
      id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb670",
      name: "Bảo hiểm Sức khoẻ",
      productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
      providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
      status: "ready",
      "__metadata__": {
      }
    },
    {
      id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb671",
      name: "Bảo hiểm vì cộng động",
      productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
      providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
      status: "ready",
    },
    {
      id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb672",
      name: "Bảo hiểm tai nạn toàn diện",
      productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
      providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
      status: "x",
    },
    {
      id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb673",
      name: "Bảo hiểm tai nạn",
      productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
      providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
      status: "x",
    },
    {
      id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb674",
      name: "Bảo hiểm ung thư",
      productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
      providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
      status: "ready",
    },
    {
      id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb675",
      name: "Bảo hiểm ung thư vú",
      productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
      providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
      status: "x",
    },
    {
      id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb676",
      name: "Bảo hiểm mô tô xe máy",
      productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
      providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
      status: "x",
    },
    {
      id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb677",
      name: "Bảo hiểm TNDS ô tô",
      productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
      providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
      status: "x",
    }
  ]
}

module.exports = { sample }
