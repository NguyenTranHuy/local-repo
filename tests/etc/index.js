'use strict';

const util = require('util');
const lodash = require('lodash');
const { DB } = require('../../engine/models');
const { InventoryManager } = require('../../engine/managers/InventoryManager');

let db = null;

function dbOpen() {
  db = db || new DB({
    "dialect": "mysql",
    "database": "ecstore",
    "username": "root",
    "password": "",
    "host": "localhost",
    "port": 3306,
    "logging": null
  });
  return db;
}

function ModelInitializer(params) {
  InventoryManager.call(this, params);

  let _sample = params && params.sample || {};

  _sample.products = lodash.filter(_sample.products, function(product) {
    return !(product && product.__metadata__ && (product.__metadata__.enabled === false));
  });
  _sample.detailProducts = lodash.filter(_sample.detailProducts, function(detailProduct) {
    return !(detailProduct && detailProduct.__metadata__ && (detailProduct.__metadata__.enabled === false));
  });
  _sample.benefits = lodash.filter(_sample.benefits, function(benefit) {
    return !(benefit && benefit.__metadata__ && (benefit.__metadata__.enabled === false));
  });
  _sample.genders = lodash.filter(_sample.genders, function(gender) {
    return !(gender && gender.__metadata__ && (gender.__metadata__.enabled === false));
  });
  _sample.productTypes = lodash.filter(_sample.productTypes, function(productType) {
    return !(productType && productType.__metadata__ && (productType.__metadata__.enabled === false));
  });
  _sample.productPackages = lodash.filter(_sample.productPackages, function(productPackage) {
    return !(productPackage && productPackage.__metadata__ && (productPackage.__metadata__.enabled === false));
  });
  _sample.informationTypes = lodash.filter(_sample.informationTypes, function(informationType) {
    return !(informationType && informationType.__metadata__ && (informationType.__metadata__.enabled === false));
  });
  _sample.providers = lodash.filter(_sample.providers, function(provider) {
    return !(provider && provider.__metadata__ && (provider.__metadata__.enabled === false));
  });

  this._sample = _sample;

  Object.defineProperty(this, "sample", {
    get: function() {
      return this._sample;
    },
    set: function(val) {
      // noop
    }
  });
}

util.inherits(ModelInitializer, InventoryManager);

ModelInitializer.prototype.importProducts = async function(opts, more) {
  opts = opts || {};
  const productInstances = {};
  const sampleProducts = filterListByMetadataCases(this._sample.products, opts.testcase);
  for(let i=0; i<sampleProducts.length; i++) {
    let item = sampleProducts[i];
    if (opts.omit_id) {
      item = lodash.omit(item, ["id"]);
    }
    let newProduct = null;
    //
    newProduct = await this.getDB(more).Product.create(item);
    //
    productInstances[newProduct.id] = newProduct;
  }
  return productInstances;
}
//
ModelInitializer.prototype.getProductStatus = async function(opts, more) {
  opts = opts || {};
  const productInstances = {};
  const sampleProducts = filterListByMetadataCases(this._sample.products, opts.testcase);
  for(let i=0; i<sampleProducts.length; i++) {
    let item = sampleProducts[i];
    if (opts.omit_id) {
      item = lodash.omit(item, ["id"]);
    }
    let newProduct = null;
    //
    newProduct = await this.getDB(more).Product.create(item);
    //
    productInstances[newProduct.id] = newProduct;
  }
  return productInstances;
}
//
ModelInitializer.prototype.importProviders = async function(opts, more) {
  opts = opts || {};
  const ProviderInstances = {};
  const sampleProviders = filterListByMetadataCases(this._sample.providers, opts.testcase);
  for(let i=0; i<sampleProviders.length; i++) {
    let item = sampleProviders[i];
    if (opts.omit_id) {
      item = lodash.omit(item, ["id"]);
    }
    let newProvider = null;
    //
    newProvider = await this.getDB(more).Provider.create(item);
    //
    ProviderInstances[newProvider.id] = newProvider;
  }
  return ProviderInstances;
}
//
ModelInitializer.prototype.importDetailProducts = async function(opts, more) {
  opts = opts || {};
  const DetailProductInstances = {};
  const sampleDetailProducts = filterListByMetadataCases(this._sample.detailProducts, opts.testcase);
  for(let i=0; i<sampleDetailProducts.length; i++) {
    let item = sampleDetailProducts[i];
    if (opts.omit_id) {
      item = lodash.omit(item, ["id"]);
    }
    let newDetailProduct = null;
    //
    newDetailProduct = await this.getDB(more).DetailProduct.create(item);
    //
    DetailProductInstances[newDetailProduct.id] = newDetailProduct;
  }
  return DetailProductInstances;
}
//
ModelInitializer.prototype.importProductTypes = async function(opts, more) {
  opts = opts || {};
  const ProductTypeInstances = {};
  const sampleProductTypes = filterListByMetadataCases(this._sample.productTypes, opts.testcase);
  for(let i=0; i<sampleProductTypes.length; i++) {
    let item = sampleProductTypes[i];
    if (opts.omit_id) {
      item = lodash.omit(item, ["id"]);
    }
    let newProductType = null;
    //
    newProductType = await this.getDB(more).ProductType.create(item);
    //
    ProductTypeInstances[newProductType.id] = newProductType;
  }
  return ProductTypeInstances;
}

ModelInitializer.prototype.importInformationTypes = async function(opts, more) {
  opts = opts || {};
  const InformationTypeInstances = {};
  const sampleInformationTypes = filterListByMetadataCases(this._sample.informationTypes, opts.testcase);
  for(let i=0; i<sampleInformationTypes.length; i++) {
    let item = sampleInformationTypes[i];
    if (opts.omit_id) {
      item = lodash.omit(item, ["id"]);
    }
    let newInformationType = null;
    //
    newInformationType = await this.getDB(more).InformationType.create(item);
    //
    InformationTypeInstances[newInformationType.id] = newInformationType;
  }
  return InformationTypeInstances;
}

ModelInitializer.prototype.importProductPackages = async function(opts, more) {
  opts = opts || {};
  const ProductPackageInstances = {};
  const sampleProductPackages = filterListByMetadataCases(this._sample.productPackages, opts.testcase);
  for(let i=0; i<sampleProductPackages.length; i++) {
    let item = sampleProductPackages[i];
    if (opts.omit_id) {
      item = lodash.omit(item, ["id"]);
    }
    let newProductPackage = null;
    //
    newProductPackage = await this.getDB(more).ProductPackage.create(item);
    //
    ProductPackageInstances[newProductPackage.id] = newProductPackage;
  }
  return ProductPackageInstances;
}

ModelInitializer.prototype.importBenefits = async function(opts, more) {
  opts = opts || {};
  const BenefitInstances = {};
  const sampleBenefits = filterListByMetadataCases(this._sample.benefits, opts.testcase);
  for(let i=0; i<sampleBenefits.length; i++) {
    let item = sampleBenefits[i];
    if (opts.omit_id) {
      item = lodash.omit(item, ["id"]);
    }
    let newBenefit = null;
    //
    newBenefit = await this.getDB(more).Benefit.create(item);
    //
    BenefitInstances[newBenefit.id] = newBenefit;
  }
  return BenefitInstances;
}

ModelInitializer.prototype.importGenders = async function(opts, more) {
  opts = opts || {};
  const GenderInstances = {};
  const sampleGenders = filterListByMetadataCases(this._sample.genders, opts.testcase);
  for(let i=0; i<sampleGenders.length; i++) {
    let item = sampleGenders[i];
    if (opts.omit_id) {
      item = lodash.omit(item, ["id"]);
    }
    let newGender = null;
    //
    newGender = await this.getDB(more).Gender.create(item);
    //
    GenderInstances[newGender.id] = newGender;
  }
  return GenderInstances;
}

function printJSON(obj) {
  console.log(JSON.stringify(obj, null, 2));
}

function mustImplement(context) {
  if (context && lodash.isFunction(context.skip)) {
    context.skip();
  } else {
    throw new Error("NotImplementedException");
  }
}

function filterListByMetadataCases(list, candidate) {
  if (!lodash.isArray(list) || list.length === 0) {
    return list;
  }
  if (!lodash.isString(candidate) && !lodash.isArray(candidate)) {
    return list;
  };
  return lodash.filter(list, function(item) {
    const metacases = lodash.get(item, "__metadata__.cases");
    if (!lodash.isArray(metacases) || metacases.length === 0) {
      return true;
    }
    if (lodash.isString(candidate) && metacases.includes(candidate)) {
      return true;
    }
    if (lodash.isArray(candidate)) {
      const intersection = lodash.intersection(metacases, candidate);
      if (intersection.length > 0) {
        return true;
      }
    }
    return false;
  })
}
//---------


//----------
module.exports = {
  ModelInitializer,
  dbOpen,
  printJSON,
  mustImplement
}