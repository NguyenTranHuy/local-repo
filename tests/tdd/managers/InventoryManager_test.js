'use strict';

const Promise = require('bluebird');
const lodash = require('lodash');
const moment = require('moment');
const chaiAsPromised = require('chai-as-promised');
const { assert, expect } = require('chai').use(chaiAsPromised);

const { ModelInitializer } = require('../../etc/index');
const { dbOpen, mustImplement, printJSON } = require('../../etc/index');

const { InventoryManager } = require('../../../engine/managers/InventoryManager');
const { sample } = require('./InventoryManager_data');

describe("[PA] InventoryManager", function() {
  //
  const modelInitializer = new ModelInitializer({ db: dbOpen(), sample });
  const inventoryManager = new InventoryManager({ db: dbOpen() });
  //
  describe("[PA-LSAR-001] InventoryManager.findProducts()", function() {
    //
    it("[PA-LSAR-001] Liệt kê được tất cả các sản phẩm hiện có trong Cơ sở dữ liệu", async function() {
      //
      await modelInitializer.resetModels();
      //
      // await modelInitializer.importDetailProducts();
      //
      await modelInitializer.importGenders();
      //
      await modelInitializer.importBenefits();  
      //
      await modelInitializer.importInformationTypes();
      //
      await modelInitializer.importProductPackages();
      //
      await modelInitializer.importProductTypes();
      //
      await modelInitializer.importProviders();
      //
      await modelInitializer.importProducts();
      //
      const result = await inventoryManager.findProducts();
      
      const products = lodash.map(result.rows, function(record) {
        return lodash.omit(record.toJSON(), ["createdAt", "updatedAt"]);
      })
      // const products = lodash.omit(result.toJSON, ["createdAt", "updatedAt"]);
      // printJSON(products);
      
      const expected = [
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb670",
          name: "Bảo hiểm Sức khoẻ",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "ready",
        },
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb671",
          name: "Bảo hiểm vì cộng động",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "ready",
        },
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb672",
          name: "Bảo hiểm tai nạn toàn diện",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "x",
        },
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb673",
          name: "Bảo hiểm tai nạn",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "x",
        },
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb674",
          name: "Bảo hiểm ung thư",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "ready",
        },
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb675",
          name: "Bảo hiểm ung thư vú",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "x",
        },
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb676",
          name: "Bảo hiểm mô tô xe máy",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "x",
        },
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb677",
          name: "Bảo hiểm TNDS ô tô",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "x",
        }
      ];
      //
      assert.equal(products.length, 8);
      assert.sameDeepMembers(products, expected);
    })

    it("[PA-LSAR-002] Liệt kê tất cả các sản phẩm sẵn sàng khi có điều kiện lọc sẵn sàng", async function() {
      // mustImplement(this);
      const record = await inventoryManager.getProduct("ready");
      const product = lodash.map(record.rows, function(record) {
        return lodash.omit(record.toJSON(), ["createdAt", "updatedAt"]);
      })
      // printJSON(record);
      // const product = lodash.omit(record.toJSON(), ["createdAt", "updatedAt"]);
      const expected = [
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb670",
          name: "Bảo hiểm Sức khoẻ",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "ready",
        },
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb671",
          name: "Bảo hiểm vì cộng động",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "ready",
        },
        {
          id: "67dc0dbe-17b2-4ae9-8ac6-78b89a0fb674",
          name: "Bảo hiểm ung thư",
          productTypeId: "1c0ded83-7220-4621-af7f-6b94b9ea4a84",
          providerId: "5c0d38e1-c06d-403e-b714-fa7767de866e",
          status: "ready",
        },
      ];
      assert.equal(product.length, 3);
      assert.sameDeepMembers(product, expected);
    })

  })
})
