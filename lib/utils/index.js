/**
  * @module utils
  */

'use strict';

const lodash = require('lodash');
const moment = require('moment');
const { newError, isPlainObject } = require('../../engine/utils');
const { BusinessOperator } = require('./_BusinessOperator');
const { HeadersExtractor } = require('./_HeadersExtractor');

const headersExtractor = new HeadersExtractor({
  rules: {
    "x-cache-enabled": {
      type: "boolean",
      path: ["__cache__", "enabled"],
    },
    "x-debug-enabled": {
      type: "boolean",
      path: ["__debug__", "enabled"],
    },
    "x-debug-delay": {
      type: "integer",
      path: ["__debug__", "delay"],
    },
    "x-debug-dryrun": {
      type: "boolean",
      path: ["__debug__", "dryrun"],
    },
    "x-debug-force-to-fail": {
      type: "boolean",
      path: ["__debug__", "forceToFail"],
    },
    "x-debug-explain-enabled": {
      type: "boolean",
      path: ["__debug__", "explainEnabled"],
    },
    "x-debug-explain-level": {
      type: "integer",
      path: ["__debug__", "explainLevel"],
    },
    "x-extra-db-profile": {
      type: "string",
      enum: [],
      path: ["__extra__", "dbProfile"],
    },
    "x-skip-json-validation": {
      type: "boolean",
      path: ["__extra__", "skipJsonValidation"],
      default: false,
    },
  }
});

const ACCESS_TOKEN_OBJECT_NAME = 'accessToken';

function extractRequest(req, reqOpts, source) {
  return {
    appType: reqOpts.clientType,
    requestId: reqOpts.requestId,
    accessTokenObject: req[ACCESS_TOKEN_OBJECT_NAME],
    ...source
  }
}

function extractMore(options, target) {
  target = target || {};
  if (!isPlainObject(options)) {
    return target;
  }
  //
  const entrypoints = headersExtractor.getEntrypoints();
  for (let i=0; i<entrypoints.length; i++) {
    const ep = entrypoints[i];
    target[ep] = options[ep];
  }
  //
  if (target.__extra__ && !lodash.isNil(target.__extra__.skipJsonValidation)) {
    target.skipValidation = target.__extra__.skipJsonValidation;
  }
  //
  target.requestId = options.requestId || undefined;
  target.operator = new BusinessOperator(options);
  //
  return target;
}

function extractCreatedAtTimeRange(query) {
  let createdAtClause = undefined;
  if (query.createdAfter) {
    createdAtClause = createdAtClause || {};
    const beginMoment = moment(query.createdAfter);
    if (!beginMoment.isValid()) {
      throw newError("InvalidTimeFormatError", {
        parameter: "createdAfter",
        value: query.createdAfter
      });
    }
    createdAtClause.beginRange = beginMoment.toDate();
  }
  if (query.createdBefore) {
    createdAtClause = createdAtClause || {};
    const endMoment = moment(query.createdBefore);
    if (!endMoment.isValid()) {
      throw newError("InvalidTimeFormatError", {
        parameter: "createdBefore",
        value: query.createdBefore
      });
    }
    createdAtClause.endRange = endMoment.toDate();
  }
  return createdAtClause;
}

function buildStatusQueryValue(query_status) {
  return lodash.isString(query_status) && query_status.length > 0 ? {"in": query_status.split(",")} : undefined
}

function buildListFromCommaString(comma_str) {
  return lodash.isString(comma_str) && comma_str.length > 0 ? comma_str.split(",") : undefined
}

function transformListOfObjectsResponse(result, req, reqOpts) {
  const resp = {
    headers: {
      'Access-Control-Expose-Headers': 'X-Total-Count',
      'X-Total-Count': result.count
    },
    body: result.rows
  };
  if (reqOpts && reqOpts.requestId) {
    resp.headers = resp.headers || {};
    resp.headers['X-Request-Id'] = reqOpts.requestId;
  }
  return resp;
}

function transformSingleObjectResponse(result, req, reqOpts) {
  const resp = {
    headers: {
      'Access-Control-Expose-Headers': 'X-Total-Count',
      'X-Total-Count': undefined
    },
    body: result
  };
  if (reqOpts && reqOpts.requestId) {
    resp.headers = resp.headers || {};
    resp.headers['X-Request-Id'] = reqOpts.requestId;
  }
  return resp;
}

module.exports = {
  headersExtractor,
  HeadersExtractor,
  extractRequest,
  extractMore,
  extractCreatedAtTimeRange,
  buildStatusQueryValue,
  buildListFromCommaString,
  transformListOfObjectsResponse,
  transformSingleObjectResponse,
};
