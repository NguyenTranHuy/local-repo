/* eslint-disable no-console */
'use strict';

const path = require('path');
const Devebot = require('devebot');
const lodash = Devebot.require('lodash');
const death = require('death');

const signtrap = lodash.once(death);

var app = require('devebot').initialize("tasks").launchApplication({
  appRootPath: __dirname,
}, [
  'app-errorlist',
  'app-restfront',
  'app-restfetch',
]);

if (require.main === module) {
  var server = app.server;
  server.start();
  signtrap(function(signal, err) {
    server.stop().then(function () {
      console.log("The server is terminated now!");
      process.exit(0);
    });
  });
}

module.exports = app;

