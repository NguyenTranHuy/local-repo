'use strict';

const lodash = require('lodash');
const { Sequelize } = require('sequelize');

function DB(params) {
  //
  let handler = null;
  function _handler() {
    if (!handler) {
      handler = new Sequelize(params);
    }
    return handler;
  }

  let def = {};

  
  //-------------------------------------------------------------------------------------------------
  // Type
  let Type = null;
  def.Type = function _Type() {
    if (Type) {
      return Type;
    }
    //
    Type = _handler().define("Type", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        field: "id",
      },
      name: {
        type: Sequelize.STRING(128),
        field: "name",
      }
    }, {
      tableName: 'Types'
    });
    //
    return Type;
  }

  //-------------------------------------------------------------------------------------------------
  // Movie
  let Movie = null;
  def.Movie = function _Movie() {
    if (Movie) {
      return Movie;
    }
    //
    Movie = _handler().define("Movie", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        field: "movieId",
      },
      name: {
        type: Sequelize.STRING(128),
        field: "movieName",
      },
      description: {
        type: Sequelize.STRING(128),
        field: "description",
      },
      image: {
        type: Sequelize.TEXT,
        field: "image",
      },
      trailer: {
        type: Sequelize.TEXT,
        field: "trailer",
      },
      director: {
        type: Sequelize.STRING(128),
        field: "director",
      },
      actor: {
        type: Sequelize.STRING(128),
        field: "actor",
      },
      // type tao ra bang rieng
      typeId: {
        type: Sequelize.STRING(128),
        field: "typeId",
      },
      length: {
        type: Sequelize.INTEGER,
        field: "length",
      },
      language: {
        type: Sequelize.STRING(128),
        field: "language",
      },
      rating: {
        type: Sequelize.INTEGER,
        field: "rating",
      },
      available: {
        type: Sequelize.BOOLEAN,
        field: "available",
      },
      dateStart: {
        type: Sequelize.DATE,
        field: "date_start",
      },
      dateEnd: {
        type: Sequelize.DATE,
        field: "date_end",
      }
    },
    {
      tableName: 'Movies'
    });
    //
    // Movie.belongsTo(def.Provider(), { as: "provider", targetKey: "id", foreignKey: "providerId" });
    // def.Provider().hasMany(Movie, { as: "Movies", targetKey: "id", foreignKey: "providerId" });

    return Movie;
  }

  //-------------------------------------------------------------------------------------------------
  // User
  let User = null;
  def.User = function _User() {
    if (User) {
      return User;
    }//
    User = _handler().define("User", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        field: "id",
      },
      name: {
        type: Sequelize.STRING(128),
        field: "name"
      }
    },
      {
        tableName: 'Users'
      });
    //
    // Ticket.belongsTo(def.Provider(), { as: "provider", targetKey: "id", foreignKey: "providerId" });
    // def.Provider().hasMany(Ticket, { as: "Tickets", targetKey: "id", foreignKey: "providerId" });
    //
    return User;
  }

  //-------------------------------------------------------------------------------------------------
  // Theater
  let Theater = null;
  def.Theater = function _Theater() {
    if (Theater) {
      return Theater;
    }//
    Theater = _handler().define("Theater", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        field: "id",
      },
      name: {
        type: Sequelize.STRING(128),
        field: "name",
      }
    },
      {
        tableName: 'Theater'
      });
    //
    // Ticket.belongsTo(def.Provider(), { as: "provider", targetKey: "id", foreignKey: "providerId" });
    // def.Provider().hasMany(Ticket, { as: "Tickets", targetKey: "id", foreignKey: "providerId" });
    //
    return Theater;
  }

  //-------------------------------------------------------------------------------------------------
  // Ticket
  let Ticket = null;
  def.Ticket = function _Ticket() {
    if (Ticket) {
      return Ticket;
    }
    //
    Ticket = _handler().define("Ticket", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        field: "id",
      },
      userId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: def.User(),
          key: "userId",
        }
      },
      theaterId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: def.Theater(),
          key: "theaterId",
        }
      },
      numberTicket: {
        type: Sequelize.INTEGER,
        field: "numberTicket",
      },
      seat: {
        type: Sequelize.ARRAY(Sequelize.STRING(64)),
        defaultValue: [],
        field: "seat",
      }
    },
      {
        tableName: 'Tickets'
      });
    //
    // Ticket.belongsTo(def.Provider(), { as: "provider", targetKey: "id", foreignKey: "providerId" });
    // def.Provider().hasMany(Ticket, { as: "Tickets", targetKey: "id", foreignKey: "providerId" });
    //
    return Ticket;
  }

  //-------------------------------------------------------------------------------------------------
  Object.defineProperties(this, {
    handler: {
      get: function () {
        return _handler();
      },
      set: function (value) { }
    },
  });

  for (let model_name in def) {
    if (lodash.isFunction(def[model_name]) && !def[model_name].disabled) {
      Object.defineProperty(this, model_name, {
        get: function () {
          return def[model_name]();
        },
        set: function (value) { }
      });
    }
  }

  this.load = function () {
    for (let model_name in def) {
      if (lodash.isFunction(def[model_name]) && !def[model_name].disabled) {
        def[model_name]();
      }
    }
    return this;
  }

  this.drop = async function (kwargs) {
    kwargs = kwargs || {};
    const handler = _handler();
    if (["development", "test"].includes(process.env.NODE_ENV)) {
      this.load();
      return await handler.drop();
    } else {
      const err = new Error("InsufficientPrivilegeError");
      err.payload = {
        nodeEnv: process.env.NODE_ENV,
        action: "DropDatabaseSchema",
      }
      throw err;
    }
  }

  /**
   * Synchronizes the Models with Tables in the database
   *
   * @returns {Promise}
   * @async
   */
  this.sync = async function (kwargs) {
    kwargs = kwargs || {};
    const handler = _handler();
    this.load();
    if (kwargs.force) {
      if (["development", "test"].includes(process.env.NODE_ENV)) {
        return await handler.sync(kwargs);
      } else {
        const err = new Error("InsufficientPrivilegeError");
        err.payload = {
          nodeEnv: process.env.NODE_ENV,
          action: "SyncDatabaseSchemas",
        }
        throw err;
      }
    }
    return await handler.sync(kwargs);
  }

  /**
   * Closes the connection
   *
   * @returns {Promise}
   * @async
   */
  this.close = function () {
    return _handler().close();
  }
}

module.exports = { DB }
